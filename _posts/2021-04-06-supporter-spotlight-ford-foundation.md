---
layout: post
title: "Supporter spotlight: Ford Foundation"
date: 2021-04-06 09:00:00
categories: org
draft: false
---

[![]({{ "/images/news/supporter-spotlight-ford-foundation/ford-foundation.png#right" | relative_url }})](https://www.fordfoundation.org/)

<big>The Reproducible Builds project relies on [several projects, supporters
and sponsors]({{ "/who/" | relative_url }}) for financial support, but they are
also valued as ambassadors who spread the word about the project and the work
that we do.</big>

This is the second instalment in a series featuring the projects, companies
and individuals who support the Reproducible Builds project. If you are a
supporter of the Reproducible Builds project (of whatever size) and would like
to be featured here, please let get in touch with us at
[contact@reproducible-builds.org](mailto:contact@reproducible-builds.org).

We started this series by [featuring the Civil Infrastructure Platform]({{ "/news/2020/10/21/supporter-spotlight-cip-project/" | relative_url }}) project, but in today's post we are speaking with
[**Michael Brennan**](https://www.fordfoundation.org/about/people/michael-brennan/),
the program officer on the *Technology and Society* team at the [**Ford
Foundation**](https://www.fordfoundation.org/).

<br>

**Chris Lamb: Hi Michael, it is great to meet you. For someone who has not
heard of the Ford Foundation before, can you give us a brief history of the
organization, and what it does today?**

Michael: The [Ford Foundation](https://www.fordfoundation.org/) is a private foundation that gives grants to
support ideas, individuals, and institutions working to advance social justice.
Our grants have supported critical organizations and activists on the front
lines of civil and human rights, the arts, poverty, and
[so much more](https://www.fordfoundation.org/about/about-ford/a-legacy-of-social-justice/).

In 1936, Edsel Ford — son of Henry, the founder of the Ford Motor
Company — established the Ford Foundation with an initial gift of $25,000. The
Ford Foundation is an independent organization, led by a distinguished board of
trustees whose 16 members hail from four continents and bring leadership and
expertise in a wide range of disciplines. Today we are stewards of a $14
billion endowment, making $500 million in grants around the world every year.
We believe that social movements are built upon individual leadership, strong
institutions, and innovative, often high-risk ideas. While the specifics of
what we work on have evolved over the years, investments in these three areas
have remained the touchstones of everything we do and are central to our theory
of how change happens in the world. These approaches have long distinguished
the Ford Foundation, and they have had a profound cumulative impact.

Across eight decades, our mission has been to reduce poverty and injustice,
strengthen democratic values, promote international cooperation, and advance
human achievement.

<br>

**Chris: Technology is often used for education and to promote justice, but it
can also amplify inequality. Is the Ford Foundation concerned with this issue
and, if so, what steps are you taking to help this?**

Technology isn't a neutral tool. It is a creation of people, systems,
organizations and societies that are anything but neutral. This means ensuring
technology promotes a more just and equitable society takes a lot of work. We
believe that our role in this is to support a diverse and sustainable field of
civil society actors that think critically about how technology can advance
justice rather than amplify inequality. The groups we support employ a wide
range of approaches in service of this goal. Some, like [Media Justice](https://mediajustice.org/),
organize and advocate for technology policy issues like net neutrality.
Programs like [Tech Congress](https://www.techcongress.io/) bring thoughtful
technologists into the halls of Congress so federal lawmakers have access to
holistic expertise on technology issues. And others, like
[Article 19](https://www.article19.org/), work on ensuring that the design of
the infrastructure that governs the internet continually centers the needs of
the public interest and not just the needs of corporations and governments.

![]({{ "/images/news/supporter-spotlight-ford-foundation/image2.png#center" | relative_url }})

As part of our current mission, we recognize the importance of technology in
undergirding all of modern society. And we believe that without just, equitable
technology policy, governance, and development, we cannot have a just society.
I am a part of the Technology and Society team, which focuses on issues of
internet freedom, digital rights and technology capacity building throughout
civil society. For example, we support the work of [Citizen Lab](https://citizenlab.ca/)
which is researching cyberattacks on activists around the world, and
organizations like [Black in AI](https://blackinai.github.io/) working to
strengthen Black representation in the field of artificial intelligence because
we believe that technology doesn't stand a chance for equitable impact without
equitable representation among people who create it.

But all of our teams recognize the importance of just approaches to technology
and incorporate that value into our grantmaking. For example, our Gender,
Racial and Ethnic Justice team focuses on criminal justice reform and the role
that algorithmic decision making has in making criminal justice systems more
unfair. And our Future of Work(ers) team engages deeply to support worker
advocacy groups around how technology is leading to the increasing
'gig-ification' of work and erosion of worker rights.

[![]({{ "/images/news/supporter-spotlight-ford-foundation/image3.jpg#center" | relative_url }})](https://blackinai.github.io/)

<br>

**Chris: Reproducible builds aims to increase the transparency of our most
important technology projects, to decentralise trust and to empower end users.
How does this fit into the objectives of the Ford Foundation?**

Foundations have long been involved in policy and economics systems that must
change in the fight for social justice. Where foundations, including Ford, have
been less engaged are in the technical systems that can have just as
significant of an impact on the lives of people around the world. Part of my
job is to identify areas in these technical systems that overlap strongly with
Ford's goals and also represent places where our grantmaking can have a
meaningful impact.

Reproducible Builds is a great example of this. We know the supply chain of
software is critical to the daily lives of most people on the planet, yet we
don't think much about the security of that supply chain compared to, say, the
ones that govern our food or our clothes. If a group like Reproducible Builds
can have a meaningful impact on making this system more trustworthy, secure and
decentralized - that's a win for the public interest, and a model we can point
to as we continue to advocate for more equitable systems that govern,
distribute and deploy the technology we all rely on every day.

<br>

**Chris: Similar to the Reproducible Builds project, the Ford Foundation
intends to have a global impact. How much do you feel being a part of a
worldwide community helps achieve your aims?**

Ford Foundation has 11 offices around the world. In some cases, it is important
for strategies and grantmaking to be focused on local or regional change. In
others, it's impossible to do that without also working on the related global
systems. Building a more just future with respect to technology requires global
coordination because, ultimately, the internet and related technologies are a
global public good that must serve us all equitably or it will fail us. I am
fortunate to get to work every day with colleagues and grantee partners around
the world working towards a common vision, even though the goals on the ground
may change day-to-day and region-to-region.

<br>

[![]({{ "/images/news/supporter-spotlight-ford-foundation/ford-foundation.png#right" | relative_url }})](https://www.fordfoundation.org/)

**Chris: If someone wanted to know more about the Ford Foundation (or even to
get involved) where should they go to look?**

We have an extensive amount of information [on our website](https://www.fordfoundation.org) — far more than I
could articulate here. To get involved, [look at the work we
support](https://www.fordfoundation.org/work/challenging-inequality/technology-and-society/).
Ford Foundation is here to help ensure frontline organizations are getting the
support they need to do their jobs. We suggest that you identify areas you are interested in,
learning what others are doing and seeing how you might be able to help. If
you want to stay more closely engaged with the Digital Infrastructure strategy
(home of grants related to Reproducible Builds), please [see their page](https://www.fordfoundation.org/work/challenging-inequality/technology-and-society/digital-infrastructure/) too.

<br>

---

*For more about the Reproducible Builds project, please see our website at
[reproducible-builds.org]({{ "/" | relative_url }}). If you are interested in
ensuring the ongoing security of the software that underpins our civilisation
and wish to sponsor the Reproducible Builds project, please reach out to the
project by emailing
[contact@reproducible-builds.org](mailto:contact@reproducible-builds.org).*
