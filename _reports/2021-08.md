---
layout: report
year: "2021"
month: "08"
title: "Reproducible Builds in August 2021"
draft: false
date: 2021-09-05 09:18:04
---

[![]({{ "/images/reports/2021-08/reproducible-builds.png#right" | relative_url }})](https://reproducible-builds.org/)

**Welcome to the latest report from the [Reproducible Builds](https://reproducible-builds.org) project.** In this post, we round up the important things that happened in the world of reproducible builds in August 2021. As always, if you are interested in contributing to the project, please visit the [*Contribute*]({{ "/contribute/" | relative_url }}) page on our website.

<br>

[![]({{ "/images/reports/2021-08/debconf21.png#right" | relative_url }})](https://debconf21.debconf.org/)

There were a large number of talks related to reproducible builds at [DebConf21](https://debconf21.debconf.org/) this year, the 21st annual conference of the [Debian](https://debian.org/) Linux distribution ([full schedule](https://debconf21.debconf.org/schedule/)):

* Firstly, Holger Levsen gave a talk titled "[*Reproducible Buster, Bullseye & Bookworm - where we come from and where we are going*](https://debconf21.debconf.org/talks/86-reproducible-buster-bullseye-bookworm-where-we-come-from-and-where-we-are-going/)" ([slides](https://reproducible-builds.org/_lfs/presentations/2021-08-24-where-we-come-from-and-where-we-are-going/)) which provided a high-level update on the status of reproducible builds within Debian, summing up the status in Debian *Buster*, *Bullseye* and then spoke about the outlook for *Bookworm*. This talk was also given at [BornHack 2021](https://bornhack.dk/bornhack-2021/) on the Danish island of [Funen](https://en.wikipedia.org/wiki/Funen) ([schedule entry](https://bornhack.dk/bornhack-2021/program/reproducible-buster-bullseye-bookworm-where-we-come-from-and-where-we-are-going/), [video](https://media.ccc.de/v/470-reproducible-buster-bullseye-bookworm-where-we-come-from-and-where-we-are-going))

* Secondly, Vagrant Cascadian gave a talk titled "[*Looking Forward to Reproducible Builds*](https://debconf21.debconf.org/talks/89-looking-forward-to-reproducible-builds/)" which mentions some historic blockers within Debian that have been solved, worked around or which are still in progress. It also touched on some recent developments, with an eye to what might happen as Debian embarks upon the new Bookworm development cycle after the release of Bullseye.

* Lastly, Johannes Schauer Marin Rodrigues & Frédéric Pierret gave a joint talk on "[*Making use of `snapshot.debian.org` for fun and profit*](https://debconf21.debconf.org/talks/22-making-use-of-snapshotdebianorg-for-fun-and-profit/)" about various tools they have developed to interact with the [*snapshot.debian.org*](https://snapshot.debian.org/) "wayback machine" for Debian packages. In particular, they mention how they are using the service to reproduce and validate builds as well as touch on an [alternative snapshot service](https://debian.notset.fr/snapshot) that has been mentioned in previous reports.

<br>

[![]({{ "/images/reports/2021-08/packagingcon.png#right" | relative_url }})](https://packaging-con.org/)

[PackagingCon](https://packaging-con.org/) ([@PackagingCon](https://twitter.com/packagingcon)) is new conference for developers of package management software as well as their related communities and stakeholders. The virtual event, which is scheduled to take place on the **9th and 10th November 2021**, has a "mission is to bring different ecosystems together: from Python’s pip to Rust’s cargo to Julia’s Pkg, from Debian apt over Nix to conda and mamba, and from vcpkg to Spack we hope to have many different approaches to package management at the conference". A number of people from reproducible builds community are planning on attending this new conference, and some may even present. Tickets start at $20 USD.

<br>

[![]({{ "/images/reports/2021-08/executive-order.png#right" | relative_url }})](https://www.whitehouse.gov/briefing-room/statements-releases/2021/08/25/fact-sheet-biden-administration-and-private-sector-leaders-announce-ambitious-initiatives-to-bolster-the-nations-cybersecurity/)

As reported in our [May report]({{ "/reports/2021-05/" | relative_url }}), the president of the United States [signed an executive order](https://www.whitehouse.gov/briefing-room/presidential-actions/2021/05/12/executive-order-on-improving-the-nations-cybersecurity/) outlining policies aimed to improve the cybersecurity in the US. The executive order comes after a number of highly-publicised security problems such as a ransomware attack that [affected an oil pipeline between Texas and New York](https://www.bbc.co.uk/news/business-57050690) and the [SolarWinds hack](https://www.theverge.com/2021/2/18/22288961/solarwinds-hack-100-companies-9-federal-agencies) that affected a large number of US federal agencies. As a followup this month, however, [a detailed fact sheet was released](https://www.whitehouse.gov/briefing-room/statements-releases/2021/08/25/fact-sheet-biden-administration-and-private-sector-leaders-announce-ambitious-initiatives-to-bolster-the-nations-cybersecurity/) announcing a number large-scale initiatives and that will undoubtedly be related to software supply chain security and, as a result, reproducible builds.

<br>

[![]({{ "/images/reports/2021-08/ircmeeting.png#right" | relative_url }})](https://lists.reproducible-builds.org/pipermail/rb-general/2021-August/002351.html)

Lastly, we ran another productive meeting on IRC in August ([original announcement](https://lists.reproducible-builds.org/pipermail/rb-general/2021-August/002351.html)) which ran for just short of two hours. A [full set of notes](http://meetbot.debian.net/reproducible-builds/2021/reproducible-builds.2021-08-31-14.59.html) from the meeting is available.

<br>

## Software development

*kpcyrd* announced an interesting new project this month called "[*I probably didn't backdoor this*](https://github.com/kpcyrd/i-probably-didnt-backdoor-this)" which is an attempt to be:

> … a practical attempt at shipping a program and having reasonably solid evidence there's probably no backdoor. All source code is annotated and there are instructions explaining how to use reproducible builds to rebuild the artifacts distributed in this repository from source.
>
> The idea is shifting the burden of proof from "you need to prove there's a backdoor" to "we need to prove there's probably no backdoor". This repository is less about code (we're going to try to keep code at a minimum actually) and instead contains technical writing that explains why these controls are effective and how to verify them. You are very welcome to adopt the techniques used here in your projects. ([…](https://github.com/kpcyrd/i-probably-didnt-backdoor-this#readme))

As the project's `README` goes on the mention: "the techniques used to rebuild the binary artifacts are only possible because the builds for this project are reproducible". This was also announced [on our mailing list](https://lists.reproducible-builds.org/pipermail/rb-general/) this month in a thread titled [*i-probably-didnt-backdoor-this: Reproducible Builds for upstreams*](https://lists.reproducible-builds.org/pipermail/rb-general/2021-August/thread.html#2343).

[![]({{ "/images/reports/2021-08/pyc.png#right" | relative_url }})](https://vulns.xyz/2021/08/reproducible-python-bytecode/)

*kpcyrd* also wrote a [detailed blog post](https://vulns.xyz/2021/08/reproducible-python-bytecode/) about the problems surrounding Linux distributions (such as [Alpine](https://www.alpinelinux.org/) and [Arch Linux](https://archlinux.org/)) that distribute compiled Python bytecode in the form of `.pyc` files generated during the build process.

<br>

#### [*diffoscope*](https://diffoscope.org)

[![]({{ "/images/reports/2021-08/diffoscope.svg#right" | relative_url }})](https://diffoscope.org)

[*diffoscope*](https://diffoscope.org) is our in-depth and content-aware diff utility. Not only can it locate and diagnose reproducibility issues, it can provide human-readable diffs from many kinds of binary formats. This month, [Chris Lamb](https://chris-lamb.co.uk) made a number of changes, including releasing [version 180](https://diffoscope.org/news/diffoscope-180-released/)), [version 181](https://diffoscope.org/news/diffoscope-181-released/)) and [version 182](https://diffoscope.org/news/diffoscope-182-released/)) as well as the following changes:

* New features:

    * Add support for extracting the [signing block](https://source.android.com/security/apksigning/v2#apk-signing-block) from Android APKs.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/69c125e4)]
    * If we specify a suffix for a temporary file or directory within the code, ensure it starts with an underscore (ie. "_") to make the generated filenames more human-readable.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/2ec50ebd)]
    * Don't include short `GCC` lines that differ on a single prefix byte either. These are distracting, not very useful and are simply the strings(1) command's idea of the build ID, which is displayed elsewhere in the diff.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/7ae6dbbb)][[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/a097e510)]
    * Don't include specific `.debug`-like lines in the ELF-related output, as it is invariably a duplicate of the debug ID that exists better in the `readelf(1)` differences for this file.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/79189fff)]

* Bug fixes:

    * Add a special case to SquashFS image extraction to not fail if we aren't the superuser.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/778fa799)]
    * Only use `java -jar /path/to/apksigner.jar` if we have an `apksigner.jar` as newer versions of `apksigner` in Debian use a shell wrapper script which will be rejected if passed directly to the JVM.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/3f8d9b33)]
    * Reduce the maximum line length for calculating Wagner-Fischer, improving the speed of output generation a lot.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/15938a18)]
    * Don't require `apksigner` in order to compare `.apk` files using `apktool`.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/38ec13ec)]
    * Update calls (and tests) for the new version of `odt2txt`.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/1e3bffc5)]

* Output improvements:

    * Mention in the output if the `apksigner` tool is missing.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/07c016a3)]
    * Profile `diffoscope.diff.linediff` and `specialize`.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/653fb1bf)][[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/5e80c6ab)]

* Logging improvements:

    * Format debug-level messages related to ELF sections using the `diffoscope.utils.format_class`.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/a0486e57)]
    * Print the size of generated reports in the logs (if possible).&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/c907d472)]
    * Include profiling information in `--debug` output if `--profile` is not set.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/21a7d8c7)]

* Codebase improvements:

    * Clarify a comment about the `HUGE_TOOLS` Python dictionary.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/0649fb3e)]
    * We can pass `-f` to apktool to avoid creating a strangely-named subdirectory.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/85e9acd2)]
    * Drop an unused `File` import.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/11b63720)]
    * Update the supported & minimum version of Black.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/4161f7fc)]
    * We don't use the `logging` variable in a specific place, so alias it to an underscore (ie. "_") instead.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/f7f4039d)]
    * Update some various copyright years.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/ee10baf8)]
    * Clarify a comment.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/79d91730)]

* Test improvements:

    * Update a test to check specific contents of SquashFS listings, otherwise it fails depending on the test systems user ID to username `passwd(5)` mapping.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/ab780bf6)]
    * Assign "seen" and "expected" values to local variables to improve contextual information in failed tests.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/6ec1c6b7)]
    * Don't print an orphan newline when the source code formatting test passes.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/80df0f6d)]

<br>

In addition Santiago Torres Arias added support for [Squashfs](https://github.com/plougher/squashfs-tools) version 4.5&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/9e410d6f)] and Felix C. Stegerman suggested a number of small improvements to the output of the new APK signing block&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/0c680ec8)]. Lastly, Chris Lamb uploaded [`python-libarchive-c`](https://tracker.debian.org/pkg/python-libarchive-c) version `3.1-1` to Debian *experimental* for the new 3.x branch — `python-libarchive-c` is used by [*diffoscope*](https://diffoscope.org/).

### Distribution work

[![]({{ "/images/reports/2021-08/debian.png#right" | relative_url }})](https://debian.org/)

In Debian, 68 reviews of packages were added, 33 were updated and 10 were removed this month, adding to our knowledge about [identified issues](https://tests.reproducible-builds.org/debian/index_issues.html). Two new issue types have been identified too: [nondeterministic_ordering_in_todo_items_collected_by_doxygen](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/6587c466) and [kodi_package_captures_build_path_in_source_filename_hash](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/f841e694).

*kpcyrd* published another [monthly report on their work on reproducible builds](https://vulns.xyz/2021/08/monthly-report/) within the [Alpine](https://www.alpinelinux.org/) and [Arch Linux](https://archlinux.org/) distributions, specifically mentioning *rebuilderd*, one of the components powering [reproducible.archlinux.org](https://reproducible.archlinux.org). The report also touches on binary transparency, an important component for supply chain security.

The [@GuixHPC](https://twitter.com/GuixHpc) account on Twitter posted an infographic on what fraction of [GNU Guix](https://guix.gnu.org/en/) packages are bit-for-bit reproducible:

[![]({{ "/images/reports/2021-08/1425802390373023745.png#center" | relative_url }})](https://twitter.com/GuixHpc/status/1425802390373023745)


[![]({{ "/images/reports/2021-08/opensuse.png#right" | relative_url }})](https://www.opensuse.org/)

Finally, Bernhard M. Wiedemann posted his [monthly reproducible builds status report](https://lists.opensuse.org/archives/list/factory@lists.opensuse.org/message/OJBHWNPNEN4HXDRTUP2UWS3SXQCIRQKV/) for openSUSE.

<br>

#### Upstream patches

The Reproducible Builds project detects, dissects and attempts to fix as many currently-unreproducible packages as possible. We endeavour to send all of our patches upstream where appropriate. This month, we wrote a large number of such patches, including:

* Bernhard M. Wiedemann:

    * [`awkward`](https://github.com/scikit-hep/awkward-1.0/pull/1050) (timestamp issue)
    * [`ck`](https://github.com/concurrencykit/ck/issues/181) (build fails in single-CPU mode)
    * [`cri-o`](https://build.opensuse.org/request/show/913227) (build ID issue related to Go parallelism)
    * [`kernel-obs-build`](https://bugzilla.opensuse.org/show_bug.cgi?id=1189305) (`cpio` metadata issue)
    * [`python-PyQt6`](https://build.opensuse.org/request/show/913014) (`.pyc`-related issue)
    * [`python-dulwich`](https://build.opensuse.org/request/show/913188) (fails to build in 2023)
    * [`python-xkbgroup`](https://build.opensuse.org/request/show/912965) (`.pyc`-related issue)
    * [`rnp`](https://github.com/rnpgp/rnp/pull/1591) (fails to build in 2024)

* Bjørn Forsman:

    * [`dosfstools`](https://github.com/dosfstools/dosfstools/pull/106) (from December 2018) was eventually merged.

* Chris Lamb:

    * [#992039](https://bugs.debian.org/992039) filed against [`mapcache`](https://tracker.debian.org/pkg/mapcache).
    * [#992059](https://bugs.debian.org/992059) filed against [`spatialindex`](https://tracker.debian.org/pkg/spatialindex).
    * [#992060](https://bugs.debian.org/992060) filed against [`pytsk`](https://tracker.debian.org/pkg/pytsk).
    * [#992061](https://bugs.debian.org/992061) filed against [`surgescript`](https://tracker.debian.org/pkg/surgescript).
    * [#992126](https://bugs.debian.org/992126) filed against [`rust-coreutils`](https://tracker.debian.org/pkg/rust-coreutils).
    * [#992772](https://bugs.debian.org/992772) filed against [`translate`](https://tracker.debian.org/pkg/translate).
    * [#992773](https://bugs.debian.org/992773) filed against [`spirv-cross`](https://tracker.debian.org/pkg/spirv-cross).
    * [#992804](https://bugs.debian.org/992804) filed against [`numcodecs`](https://tracker.debian.org/pkg/numcodecs).
    * [#993279](https://bugs.debian.org/993279) filed against [`tty-solitaire`](https://tracker.debian.org/pkg/tty-solitaire).
    * [#993304](https://bugs.debian.org/993304) filed against [`samtools`](https://tracker.debian.org/pkg/samtools).

* Simon McVittie:

    * [#992620](https://bugs.debian.org/992620) filed against [`pkg-config`](https://tracker.debian.org/pkg/pkg-config).
    * [#992622](https://bugs.debian.org/992622) filed against [`pkgconf`](https://tracker.debian.org/pkg/pkgconf).
    * [#992645](https://bugs.debian.org/992645) filed against [`ncftp`](https://tracker.debian.org/pkg/ncftp).
    * [#992647](https://bugs.debian.org/992647) filed against [`backuppc`](https://tracker.debian.org/pkg/backuppc).
    * [#992651](https://bugs.debian.org/992651) filed against [`sharutils`](https://tracker.debian.org/pkg/sharutils).
    * [#992662](https://bugs.debian.org/992662) filed against [`cfengine3`](https://tracker.debian.org/pkg/cfengine3).
    * [#992702](https://bugs.debian.org/992702) filed against [`nbdkit`](https://tracker.debian.org/pkg/nbdkit).
    * [#992722](https://bugs.debian.org/992722) filed against [`nbdkit`](https://tracker.debian.org/pkg/nbdkit).
    * [#992775](https://bugs.debian.org/992775) filed against [`python3.9`](https://tracker.debian.org/pkg/python3.9).
    * [#992781](https://bugs.debian.org/992781) filed against [`supermin`](https://tracker.debian.org/pkg/supermin).
    * [#992782](https://bugs.debian.org/992782) filed against [`virt-p2v`](https://tracker.debian.org/pkg/virt-p2v).
    * [#993249](https://bugs.debian.org/993249) filed against [`gnunet`](https://tracker.debian.org/pkg/gnunet).
    * [#993250](https://bugs.debian.org/993250) filed against [`mpb`](https://tracker.debian.org/pkg/mpb).
    * [#993275](https://bugs.debian.org/993275) filed against [`ng`](https://tracker.debian.org/pkg/ng).

* Vagrant Cascadian:

    * [`vlc`](https://code.videolan.org/videolan/vlc/-/issues/26035) (`libvlccore.so.*` embeds the build username and hostname).
    * [#991926](https://bugs.debian.org/991926), [#991927](https://bugs.debian.org/991927) and [#991928](https://bugs.debian.org/991928) filed against [`grub2`](https://tracker.debian.org/pkg/grub2).

[![]({{ "/images/reports/2021-08/android.png#right" | relative_url }})](https://issuetracker.google.com/issues/195968520)

Elsewhere, it was discovered that when supporting various new language features and APIs for Android apps, the resulting APK files that are generated [now vary wildly from build to build](https://issuetracker.google.com/issues/195968520) ([example *diffoscope* output](https://salsa.debian.org/reproducible-builds/diffoscope/-/issues/272)). Happily, it appears that [a patch has been committed to the relevant source tree](https://issuetracker.google.com/issues/195968520#comment14). This was also discussed [on our mailing list](https://lists.reproducible-builds.org/pipermail/rb-general/) this month in a thread titled [*Android desugaring and reproducible builds*](https://lists.reproducible-builds.org/pipermail/rb-general/2021-August/thread.html#2332) started by Marcus Hoffmann.

<br>

### Website and documentation

[![]({{ "/images/reports/2021-08/website.png#right" | relative_url }})](https://reproducible-builds.org/)

There were quite a few changes to the [Reproducible Builds website and documentation](https://reproducible-builds.org/) this month, including:

* Felix C. Stegerman:

    * Update the website self-build process to not use the `buster-backports` suite now that Debian *Bullseye* is the stable release.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/aa39389)]

* Holger Levsen:

    * Add a new page documenting various package rebuilder solutions.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/e45a428)]
    * Add some historical talks and slides from [DebConf20](https://debconf20.debconf.org).&nbsp;[[…](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/46c4582)][[…](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/1347672)]
    * Various improvements to the "history" page.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/61df505)][[…](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/58dfc7b)][[…](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/cb588bc)]
    * Rename the "Comparison protocol" documentation category to "Verification".&nbsp;[[…](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/3bc5cb5)]
    * Update links to F-Droid documentation.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/d45169b)]

* Ian Muchina:

    * Increase the font size of titles and de-emphasize event details on the talk page.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/293fd40)]
    * Rename the `README` file to `README.md` to improve the user experience when browsing the Git repository in a web browser.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/003e04c)]

* Mattia Rizzolo:

    * Drop a `position:fixed` CSS statement that is negatively affecting with some width settings.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/6196b9c)]
    * Fix the sizing of the elements inside the side navigation bar.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/58a8bec)]
    * Show gold level sponsors and above in the sidebar.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/d6049f4)]
    * Updated the documentation within `reprotest` to mention how `ldconfig` conflicts with the kernel variation.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/reprotest/commit/5ea1f68)]

* Roland Clobus:

    * Added a ticket number for the issue with the "live" Cinnamon image and [*diffoscope*](https://diffoscope.org).&nbsp;[[…](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/b3009cd)]

#### Testing framework

[![]({{ "/images/reports/2021-08/testframework.png#right" | relative_url }})](https://tests.reproducible-builds.org/)

The Reproducible Builds project runs a testing framework at [tests.reproducible-builds.org](https://tests.reproducible-builds.org), to check packages and other artifacts for reproducibility. This month, the following changes were made:

* Holger Levsen:

    * [Debian](https://debian.org)-related changes:

        * Make a large number of changes to support the new Debian *bookworm* release, including adding it to the dashboard&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/050159ca)], start scheduling tests&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/1c5b2efa)], adding suitable Apache redirects&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/2a9ea663)] etc.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/e5eb75ad)][[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/7cdc13c8)][[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/c7323eb5)][[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/4a114b33)][[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/9229cfe2)]
        * Make the first build use `LANG=C.UTF-8` to match the official Debian build servers.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/ba32c655)]
        * Only test Debian Live images once a week.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/a6e91e58)]
        * Upgrade all nodes to use Debian *Bullseye*&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/4ea45d99)] [[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/0409a90c)]
        * Update README documentation for the Debian *Bullseye* release.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/7eb576fc)]

    * Other changes:

        * Only include [`rsync`](https://rsync.samba.org/) output if the `$DEBUG` variable is enabled.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/023110e7)]
        * Don't try to install `mock`, a tool used to build Fedora packages some time ago.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/9538c6d6)]
        * Drop an unused function.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/e255b6e8)]
        * Various documentation improvements.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/a0560508)][[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/d5de3114)]
        * Improve the node health check to detect "zombie" jobs.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/1f5606a0)]

* Jessica Clarke ([FreeBSD](https://www.freebsd.org/)-related changes):

    * Update the location and branch name for the main FreeBSD Git repository.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/b00543b5)]
    * Correctly ignore the source tarball when comparing build results.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/2d1468a2)]
    * Drop an outdated version number from the documentation.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/a373d301)]

* Mattia Rizzolo:

    * Block F-Droid jobs from running whilst the setup is running.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/b6713424)]
    * Enable debugging for the [`rsync`](https://rsync.samba.org/) job related to Debian Live images.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/77b3ce6a)]
    * Pass `BUILD_TAG` and `BUILD_URL` environment for the Debian Live jobs.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/1734f0a7)]
    * Refactor the `master_wrapper` script to use a Bash array for the parameters.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/80eb2417)]
    * Prefer YAML's `safe_load()` function over the "unsafe" variant.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/cb6c7a43)]
    * Use the correct variable in the Apache config to match possible existing files on disk.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/29c25838)]
    * Stop issuing HTTP 301 redirects for things that not actually permanent.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/e8c9ac58)]

* Roland Clobus (Debian "live" image generation):

    * Increase the [*diffoscope*](https://diffoscope.org) timeout from 120 to 240 minutes; the Cinnamon image should now be able to finish.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/239e3fdd)]
    * Use the new snapshot service.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/c266ac41)]
    * Make a number of improvements to artifact handling, such as moving the artifacts to the Jenkins host&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/7da991d1)] and correctly cleaning them up at the right time.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/7d74e95c)][[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/66bb1a36)][[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/e59f4953)]
    * Where possible, link to the Jenkins build URL that created the artifacts.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/b1aca78c)][[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/effd095e)]
    * Only allow only one job to run at the same time.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/e982a5d3)]

* Vagrant Cascadian:

    * Temporarily disable `armhf` nodes for [DebConf21](https://debconf21.debian.org).&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/2466fb30)][[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/491ec0bb)]

<br>

Lastly, if you are interested in contributing to the Reproducible Builds project, please visit the [*Contribute*](https://reproducible-builds.org/contribute/) page on our website. You can get in touch with us via:

 * IRC: `#reproducible-builds` on `irc.oftc.net`.

 * Twitter ([@ReproBuilds](https://twitter.com/ReproBuilds)) and Mastodon ([@reproducible_builds@fosstodon.org](https://fosstodon.org/@reproducible_builds)).

 * Reddit: [/r/ReproducibleBuilds](https://reddit.com/r/reproduciblebuilds)

 * Mailing list: [`rb-general@lists.reproducible-builds.org`](https://lists.reproducible-builds.org/listinfo/rb-general)
